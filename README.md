# Prueba de ingreso 

Cabe aclarar que dentro de mis capacidades y experiencia está implementar proyectos de arquitectura orientados a los servicios en servicios de computacion en la nube como Google Cloud o AWS, ademas de esto mis estudios me han dado la capacidad de manejar Big Data y Analisis de datos.

Para probar el proyecto, se recuerda usar un servidor que cumpla con las especificaciones tecnicas mencionadas mas adelante.

Cuadrangular

El presente proyecto consiste en un programa que registre equipos, registre partidos y muestre tablas de posiciones para
un cuadrangular, el cual consiste en 4 equipos que se enfrentan todos con todos y de esta manera genera cierta puntuacion.

El proyecto no se centro en aspectos de estetica, ya que lo primero y mas importante es su funcionalidad.
Con mas tiempo se puede mejorar aspectos visuales.

Aspectos Tecnicos

El programa se desarrollo en html5, php y utilizando mysql para la gestion de datos.

Se utilizo XAMPP para simular un servidor con las siguientes caracteristicas:

Servidor: 127.0.0.1 via TCP/IP
Tipo de servidor: MariaDB
Conexión del servidor: No se está utilizando SSL Documentación
Versión del servidor: 10.4.11-MariaDB - mariadb.org binary distribution
Versión del protocolo: 10
Usuario: root@localhost
Conjunto de caracteres del servidor: UTF-8 Unicode (utf8mb4)

Apache/2.4.43 (Win64) OpenSSL/1.1.1g PHP/7.4.6
Versión del cliente de base de datos: libmysql - mysqlnd 7.4.6
extensión PHP: mysqli Documentación curl Documentación mbstring Documentación
Versión de PHP: 7.4.6

Este proyecto se puede implementar en un servidor en la nube como Google Cloud o AWS. Creando una maquina virtual, realizando las respectivas instalciones de las herramientas
que se necesitan, y enlazando un host al servidor.

Realizado por

Edward Steven Camelo Castillo
