-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generaci�n: 03-11-2017 a las 20:40:44
-- Versi�n del servidor: 10.1.19-MariaDB
-- Versi�n de PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;



CREATE TABLE `cuadrangular` (
  `id_use` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `entrenador` varchar(100) NOT NULL,
  `facultad` varchar(100) NOT NULL,
  `puntaje` int(11) NOT NULL DEFAULT '0'

  
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `cuadrangular`
  ADD PRIMARY KEY (`id_use`);

ALTER TABLE `cuadrangular`
  MODIFY `id_use` int(1) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;