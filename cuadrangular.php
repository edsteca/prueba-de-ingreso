<?php
$host = "localhost";
$dbuser = "prueba";
$dbpwd = "distrital12.";
$db = "distrital";

$connect = mysqli_connect ($host, $dbuser, $dbpwd);
	if(!$connect)
		echo ("No se ha conectado a la base de datos");
	else
		$select = mysqli_select_db ($connect, $db);
?>
<?php
////////////// Consulta Base de Datos ///////////////////////
$equipos = mysqli_query($connect, "SELECT * FROM cuadrangular ");
$ordenados = mysqli_query($connect, "SELECT * FROM cuadrangular ORDER BY puntaje DESC ");
?>
<!DOCTYPE html>
<html>
<head>
    <title> Cuadrangular</title>
</head>
<body background="img/background.jpg">
    <div>
        <section>
            <div class="col-md-12">
                <h1 align="center" style="color:white;">Copa Universidad Distrital Francisco Jose de Caldas.</h1>
                <p align="center" style="color:white;">Cuadrangular</p>
            </div>
        </section>
        <hr><br />
        <!--  Registro Equipos -->
        <section>
            <section>
                <h3 style="color:white;">Registro Equipo</h3>
                <p></p>
            </section>
        </section>
        <section>
            <form action="" method="post">
                <section>
                    <section>
                        <div>
                            <div >
                                <label for="nombre">Nombre del equipo: *</label>
                                <input type="text" class="form-control" name="nombre" maxlength="128" placeholder="Nombre del Equipo" required>
                            </div>
                        </div>

                    </section>
                    <section>
                        <div>
                            <div>
                                <label for="entrenador">Nombre del entrenador: *</label>
                                <input type="text" class="form-control" name="entrenador" maxlength="128" placeholder="Nombre del entrenador" required>
                            </div>
                        </div>

                    </section>
                    <section>
                        <div>
                            <label for="facultad">Facultad: *</label>
                            <select  name="facultad">
                                <option value="Ingenieria">Facultad de Ingenieria</option>
                                <option value="Ciencias">Facultad de Ciencias y Educación</option>
                                <option value="Artes">Facultad de Artes</option>
                                <option value="Ambiental">Facultad de Medio Ambiente</option>

                            </select>
                        </div>
                    </section>
                    <section>
                        <button type="submit" name="guardar" >Registrar Equipo</button>
                    </section>
                </section>
            </form>
            <?php
                if(isset($_POST['guardar'])) {
                $nombre= mysqli_real_escape_string($connect,$_POST['nombre']);
                $entrenador = mysqli_real_escape_string($connect,$_POST['entrenador']);
                $facultad = mysqli_real_escape_string($connect,$_POST['facultad']);

                $queryuser = mysqli_query($connect, "SELECT nombre FROM cuadrangular ");
                $comprobarusuario = mysqli_num_rows($queryuser);

                if($comprobarusuario >= 4) { ?>

                <br>
                <div >
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                Los equipos estan completos
                </div>

                <?php } else {
                   $insertar = mysqli_query($connect, "INSERT INTO cuadrangular (nombre,entrenador,facultad) values ('$nombre','$entrenador','$facultad')");

                        if($insertar) { ?>

                        <br>
                        <div >
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Felicidades ha registrado correctamente el equipo
                        </div>

                        <?php

                        header("Refresh: 2; url = cuadrangular.php");

                        }
                        else {?>

                                <br>
                                <div>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                El registro no se ha completo
                                </div>

                                <?php
       
			            }

                }
                }



            ?>

        </section>
        <hr />

        <!--  Equipos  -->
        <section>
            <div>
                <h3 style="color:white;">Lista de Equipos registrados.</h3 style="color:white;">
                <p></p>
            </div>
        </section>
        <section class="row">
            
            <table>
                <thead>
                <tr>
                    <th style="color:white;" scope="col">Equipo</th>
                    <th style="color:white;" scope="col">Nombre de Equipo</th>
                </tr>
                </thead >
                <tbody id="datos">
                <?php
                while ($registroEquipos = $equipos->fetch_array(MYSQLI_BOTH)){ 
                
                echo'<tr>
                    <td style="color:yellow;">'.$registroEquipos['id_use'].'</td>
                    <td style="color:yellow;">'.$registroEquipos['nombre'].'</td>
                    </tr>';
                }

                ?>
                </tbody>

                
                 
                
            </table>
        </section>

        


        <br />
        <hr />
        <!--  Satisfacción General  -->
        <section class="row">
            <div class="col-md-12">
                <h3 style="color:white;">Registrar Partido.</h3>
                <p></p>
            </div>
        </section>
        <!--  Registrar Partido -->
        <section >
            <form action="" method="post">
                <div>
                    <section>
                        <div>
                            <div class="form-group">
                                <label style="color:pink;" for="equipoa">Equipo A (Numero) : *</label>
                                <input type="number" class="form-control" name="equipoa" maxlength="1" placeholder="Numero de Equipo" required>
                            </div>
                        </div>

                    </section>
                    <section>
                        <div>
                            <div>
                                <label style="color:pink;" for="marcadora">Marcador Equipo A: *</label>
                                <input type="number" class="form-control" name="marcadora" maxlength="2" placeholder="Marcador Equipo A" required>
                            </div>
                        </div>

                    </section>
                    <section>
                        <div>
                            <div class="form-group">
                                <label style="color:orange;" for="equipob">Equipo B (Numero) *</label>
                                <input type="number" class="form-control" name="equipob" maxlength="1" placeholder="Numero del Equipo" required>
                            </div>
                        </div>

                    </section>
                    <section>
                        <div>
                            <div>
                                <label style="color:orange;" for="marcadorb">Marcador Equipo B: *</label>
                                <input type="number" class="form-control" name="marcadorb" maxlength="2" placeholder="Marcador Equipo B" required>
                            </div>
                        </div>

                    </section>
                    <section>
                        <button type="submit" name="regpartido">Registrar Partido</button>
                    </section>
                </div>
            </form>
            <?php
                if(isset($_POST['regpartido'])) {
                $equipoa= mysqli_real_escape_string($connect,$_POST['equipoa']);
                $equipob = mysqli_real_escape_string($connect,$_POST['equipob']);
                $marcadora = mysqli_real_escape_string($connect,$_POST['marcadora']);
                $marcadorb = mysqli_real_escape_string($connect,$_POST['marcadorb']);
                $ganador = 3;
                $empate = 1;

                if( $marcadora > $marcadorb){
                        
                        $cons = mysqli_query($connect, "SELECT * FROM cuadrangular WHERE id_use = '$equipoa'");
                        $row=mysqli_fetch_array($cons);
                        $actual=$row['puntaje'];

                        $resul = $actual + $ganador;

                        $sql = mysqli_query($connect, "UPDATE cuadrangular SET puntaje = '$resul' WHERE id_use = '$equipoa'");
                        if($sql){?>

                        <br>
                        <div >
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Felicidades ha registrado el partido exitosamente
                        </div>
                        <meta http-equiv="Refresh" content="5;url=cuadrangular.php">
                        <?php

                       



                        }else{?>

                                <br>
                                <div>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                El registro del partido no se hizo
                                </div>

                                <?php

                            }
                        }
                if( $marcadora < $marcadorb){
                        $cons = mysqli_query($connect, "SELECT * FROM cuadrangular WHERE id_use = '$equipob'");
                        $row=mysqli_fetch_array($cons);
                        $actual=$row['puntaje'];

                        $resul = $actual + $ganador;
                        
                        $sql = mysqli_query($connect, "UPDATE cuadrangular SET puntaje = '$resul' WHERE id_use = '$equipob'");
                        if($sql){?>

                        <br>
                        <div >
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Felicidades ha registrado el partido exitosamente
                        </div>
                        <meta http-equiv="Refresh" content="5;url=cuadrangular.php">
                        <?php
                        

                        }else{?>

                                <br>
                                <div>
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                El registro del partido no se hizo
                                </div>

                                <?php

                            }
                        }
                if( $marcadora == $marcadorb){
                        $cons1 = mysqli_query($connect, "SELECT * FROM cuadrangular WHERE id_use = '$equipoa'");
                        $row1=mysqli_fetch_array($cons1);
                        $actual1=$row1['puntaje'];

                        $resul1 = $actual1 + $empate;
                        $sql1 = mysqli_query($connect, "UPDATE cuadrangular SET puntaje = '$resul1' WHERE id_use = '$equipoa'");
                       
                        $cons2 = mysqli_query($connect, "SELECT * FROM cuadrangular WHERE id_use = '$equipob'");
                        $row2=mysqli_fetch_array($cons2);
                        $actual2=$row2['puntaje'];

                        $resul2 = $actual2 + $empate;


                        $sql2 = mysqli_query($connect, "UPDATE cuadrangular SET puntaje = '$resul2' WHERE id_use = '$equipob'");
              
                        if($sql1){?>

                        <br>
                        <div style="color:white;" >
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            Felicidades ha registrado el partido exitosamente
                        </div>
                        <meta http-equiv="Refresh" content="5;url=cuadrangular.php">
                        <?php

                        



                        }else{?>

                                <br>
                                <div style="color:white;">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                El registro del partido no se hizo
                                </div>

                                <?php

                            }
                        }

               }

            ?>

            <!--  Satisfacción General  -->
        <section class="row">
            <div class="col-md-12">
                <h3 style="color:white;">Tabla de Posiciones.</h3>
                <p></p>
            </div>
        </section>
        <section class="row">
            
            <table>
                <thead>
                <tr>
                    <th style="color:white;" scope="col">Numero de Equipo</th>
                    <th style="color:white;" scope="col">Nombre de Equipo</th>
                    <th style="color:white;" scope="col">Puntaje</th>
                </tr>
                </thead >
                <tbody id="datos">
                <?php
                while ($registroEquipos = $ordenados->fetch_array(MYSQLI_BOTH)){ 
                
                echo'<tr>
                    <td style="color:yellow;">'.$registroEquipos['id_use'].'</td>
                    <td style="color:yellow;">'.$registroEquipos['nombre'].'</td>
                    <td style="color:yellow;">'.$registroEquipos['puntaje'].'</td>
                    </tr>';
                }

                ?>
                </tbody>

                
                 
                
            </table>
        </section>


        </section><br />
        
    <footer class="container">
        <p style="color:orange;">Todos los derechos reservados para Edward Steven Camelo Castillo.</p>
    </footer>
</body>
</html>